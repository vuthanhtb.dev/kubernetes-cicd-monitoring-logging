### [kind tool](https://kind.sigs.k8s.io/docs/user/quick-start/)
### Create cluster
```sh
kind create cluster --name=local-cluster --config=kind/config.yaml
```

### Get cluster
```sh
kind get clusters
```

### Delete cluster
```sh
kind delete cluster --name=local-cluster
```

### Metric-server
```sh
kubectl apply -f metrics-server/config.yml
```
#### Errors throw when connected to kubectl
```sh
kubectl -n kube-system logs metrics-server-6757d65f8-tfwb5
```
message: Error from server: Get "https://172.18.0.3:10250/containerLogs/kube-system/metrics-server-6757d65f8-tfwb5/metrics-server": remote error: tls: internal error

#### Fix: Approve kubelet certificate requests
```sh
for kubeletcsr in `kubectl -n kube-system get csr | grep kubernetes.io/kubelet-serving | awk '{ print $1 }'`; do kubectl certificate approve $kubeletcsr; done
```

### POD
#### port-forward
```sh
kubectl port-forward pod/react-app-pod 9000:80
```
test: http://localhost:9000/

### Namespaces & DNS
#### dns: <SERVICE_NAME>.<NAMESPACE_NAME>.svc.cluster.local
### Setting Default Namespace
```sh
kubectl config set-context --current --namespace=<NAMESPACE>
```

### Label
#### Create label node
```sh
kubectl label nodes local-cluster-worker3 tier=prod
```
