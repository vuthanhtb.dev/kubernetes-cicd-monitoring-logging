#!bin/sh

#Generate a Private Key and a CSR

openssl req \
       -newkey rsa:2048 -nodes -keyout domain.key \
       -out domain.csr



#Generate a CSR from an Existing Private Key
openssl req \
       -key domain.key \
       -new -out domain.csr


openssl req -new -sha256 -key mydomain.com.key -subj "/C=US/ST=CA/O=MyOrg, Inc./CN=mydomain.com" -out mydomain.com.csr



#Generate a Self-Signed Certificate
openssl req \
       -newkey rsa:2048 -nodes -keyout domain.key \
       -x509 -days 365 -out domain.crt


#Generate a Self-Signed Certificate from an Existing Private Key
openssl req \
       -key domain.key \
       -new \
       -x509 -days 365 -out domain.crt


#Generate a Self-Signed Certificate from an Existing Private Key and CSR
openssl x509 \
       -signkey domain.key \
       -in domain.csr \
       -req -days 365 -out domain.crt

#View CSR Entries
openssl req -text -noout -verify -in domain.csr


#View Certificate Entries
openssl x509 -text -noout -in domain.crt



#Verify a Certificate was Signed by a CA
openssl verify -verbose -CAFile ca.crt domain.crt




#Generate the certificate using the mydomain csr and key along with the CA Root key
openssl x509 -req -in mydomain.com.csr -CA rootCA.crt -CAkey rootCA.key -CAcreateserial -out mydomain.com.crt -days 500 -sha256
